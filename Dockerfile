# You should always specify a full version here to ensure all of your developers
# are running the same version of Node.
FROM node:8

# The base node image sets a very verbose log level.
ENV NPM_CONFIG_LOGLEVEL warn
# 3. Add the current directory . into the path /usr/app in the image.
ADD . /usr/app
WORKDIR /usr/app
RUN chmod -R -f 777 /usr/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package.json ./

#RUN npm install



# Copy all local files into the image.
COPY . .

# Build for production.
#RUN npm run build --production

# Install `serve` to run the application.
RUN npm install -g serve

# Set the command to start the node server.
#CMD serve -s build

# Tell Docker about the port we'll run on.
EXPOSE 5000